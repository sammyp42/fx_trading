import pickle
from candle import Price, Candle
from datetime import datetime
import sqlite3

fc = []

with open('outfile.can', 'rb') as fp:
    fc = pickle.load(fp)
print("loaded pickled data!")

conn = sqlite3.connect("cdatat.sqlite")
cursor = conn.cursor()

cursor.execute("drop table samp")
cursor.execute("create table samp (data TEXT)")
conn.commit()

q = "INSERT INTO samp VALUES (?)"

i = 1
for c in fc:
    cursor.execute(q, (pickle.dumps(c),))
    if i % 10000 == 0:
        conn.commit()
        print("completed " + str(i))
    i += 1

conn.commit()

q = "SELECT data from samp"
cursor.execute(q)

i = 1
while True:
    d = cursor.fetchmany(size=10000)
    if not d:
        break
    for c in d:
        can = pickle.loads(c[0])
        if i % 2500 == 0:
            print(can.time, can.bid.l, can.ask.h)
        i += 1

conn.close()
