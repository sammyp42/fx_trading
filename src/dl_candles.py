import sys
import argparse
import common.config
import pickle
from candle import Price, Candle
from datetime import datetime
import db

def main():
    """
    Create an API context, and use it to fetch and display the state of an
    Account.

    The configuration for the context and Account to fetch is parsed from the
    config file provided as an argument.
    """

"""
REPLACE BELOW WITH SAMPLE code from

C:\\src\\v20-python-samples\\src\\market_order_full_example
"""


#     parser = argparse.ArgumentParser()

#     #
#     # The config object is initialized by the argument parser, and contains
#     # the REST APID host, port, accountID, etc.
#     #
#     common.config.add_argument(parser)

#     args = parser.parse_args()

#     #
#     # The v20 config object creates the v20.Context for us based on the
#     # contents of the config file.
#     #
#     api = args.config.create_context()

#     account_id = args.config.active_account
#     response = api.account.get(account_id)

# #    print(response)

#     response = response.get("account", "200")



    # #
    # # Fetch the details of the Account found in the config file
    # #

    # fmt = "%Y-%m-%d %H:%M:%S"
    # dt = datetime.strptime("2005-01-01 00:00:00", fmt)

    # inst = "EUR_USD"
    # kwargs = {}
    # kwargs["granularity"] = "S5"
    # kwargs["price"] = "BMA"
    # kwargs["fromTime"] = api.datetime_to_str(dt)
    # kwargs["count"] = 5000
    # kwargs["includeFirst"] = True

    # response = api.instrument.candles(inst, **kwargs)

    # fc = []

    # for c in response.get("candles", 200):
    #     bidP = Price(c.bid.c, c.bid.h, c.bid.l, c.bid.o)
    #     askP = Price(c.ask.c, c.ask.h, c.ask.l, c.ask.o)
    #     midP = Price(c.mid.c, c.mid.h, c.mid.l, c.mid.o)
    #     candle = Candle(inst, c.time, bidP, askP, midP, c.volume)
    #     fc.append(candle)


    # for _ in range(100):
    #     lastT = str(
    #         datetime.strptime(
    #             fc[-1].time,
    #             "%Y-%m-%dT%H:%M:%S.000000000Z"
    #         )
    #     )

    #     print(lastT, " size:", len(fc), " bytes:", sys.getsizeof(fc))
    #     kwargs["includeFirst"] = False

    #     dt = datetime.strptime(lastT, fmt)
    #     kwargs["fromTime"] = api.datetime_to_str(dt)

    #     response = api.instrument.candles(inst, **kwargs)

    #     for c in response.get("candles", 200):
    #         bidP = Price(c.bid.c, c.bid.h, c.bid.l, c.bid.o)
    #         askP = Price(c.ask.c, c.ask.h, c.ask.l, c.ask.o)
    #         midP = Price(c.mid.c, c.mid.h, c.mid.l, c.mid.o)
    #         candle = Candle(inst, c.time, bidP, askP, midP, c.volume)
    #         fc.append(candle)

    # lastT = str(
    #     datetime.strptime(
    #         fc[-1].time,
    #         "%Y-%m-%dT%H:%M:%S.000000000Z"
    #     )
    # )

    # print(lastT, " size:", len(fc), " bytes:", sys.getsizeof(fc))

#    with open('outfile.can', 'wb') as fp:
#        pickle.dump(fc, fp)

if __name__ == "__main__":
    main()
