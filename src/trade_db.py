import sys
import sqlite3
from operator import itemgetter
from sqlite3 import Error


class Trades:
    UP = 1
    DOWN = -1
    d = {UP: "up", DOWN: "down"}

    _dtfmt = "%Y-%m-%d %H:%M:%S.000000000Z"

    __T_TABLES = {"up", "down", "closed"}
    # rowid is implicit pkey
    __T_FIELDS = {"quant": " INTEGER",
                  "open": " REAL", "close": " REAL",
                  "open_ex": " REAL", "close_ex": " REAL",
                  "dt_o": " TEXT", "dt_c": " TEXT"}

    def __init__(self, name):
        print("Opening sqlite3 file:", name)
        self.__conn = sqlite3.connect(name)
        self.__cursor = self.__conn.cursor()
        if self._tables_exist():
            print("DB Exists!")
        else:
            print("Creating DB...")
            self._create_tables()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__conn.commit()
        self.__conn.close()
        print("exiting Trades")

    def _execute(self, q, v=None):
        if not v:
            self.__cursor.execute(q)
        else:
            self.__cursor.execute(q, v)
        self.__conn.commit()

    def _tables_exist(self):
        q = "select name from sqlite_master where type='table'"
        r = self.__cursor.execute(q).fetchall()
        rt = {t[0] for t in r}
        return(self.__T_TABLES.issubset(rt))

    def _create_tables(self):
        print("Creating tables!")
        for t in self.__T_TABLES:
            ct = "CREATE TABLE IF NOT EXISTS " + t + "("
            for f in self.__T_FIELDS:
                ct += f + " " + self.__T_FIELDS[f] + ", "
            ct = ct[0:-2] + ")"
            self._execute(ct)

    # assumes a select query was executed
    def _get_trades(self):
        r = self.__cursor.fetchall()
        return r

    # create a trade, d is either UP (1), or DOWN (-1)
    def create_trade(self, mov, quant, price, d):
        q = "INSERT INTO " + self.d[d] + " VALUES (?, ?, ?, ?, ?, ?, ?)"
        v = ((quant),
             ((round(price, 6))),
             ((round(price + ((mov / 10000) * d), 6))),
             '', '', '', '')
        self._execute(q, v)

    def get_trades(self, d, t_o=3):
        """ get a list of trade tuples, sorted by closest to furthest

        args:
            t_o: 0 - unopened
                 1 - open date set
                 2 - close date set
                 3 - all (default)
        """

        q = "SELECT rowid, * FROM " + self.d[d]

        q_t = {0: " WHERE dt_o = \"\" and dt_c = \"\" ",
               1: " WHERE dt_o != \"\" and dt_c = \"\" ",
               2: " WHERE dt_c != \"\" ",
               3: " "}

        q += q_t[t_o]

        self._execute(q)
        t = self._get_trades()

        if d == self.UP:
            t.sort(key=itemgetter(2), reverse=False)
        else:
            t.sort(key=itemgetter(2), reverse=True)
        return t

    def _check_trade(self, rowid, d, dt_t):
        q = "SELECT * FROM " + self.d[d]
        q += " WHERE rowid = ? AND " + dt_t + " != \"\""
        self._execute(q, (rowid,))
        return(len(self._get_trades()))

    def open_trade(self, d, rowid, price, date):
        """
        open_trade(self, d, rowid, price, date)
        """
        if self._check_trade(rowid, d, "dt_o") == 1:
            raise Error("Trying to OPEN TRADE that is ALREADY OPEN!!")

        q = "UPDATE " + self.d[d] + " "
        q += "SET open_ex = ?, dt_o = ? "
        q += "WHERE rowid = ? "
        self._execute(q, (price, date, rowid))

    def close_trade(self, d, rowid, price, date):
        """
        close_trade(self, d, rowid, price, date)
        """
        if self._check_trade(rowid, d, "dt_o") == 0:
            raise Error("Trying to CLOSE TRADE that WAS NOT OPENED!!")

        if self._check_trade(rowid, d, "dt_c") == 1:
            raise Error("Trying to CLOSE TRADE that IS ALREADY CLOSED!!")

        q = "UPDATE " + self.d[d] + " "
        q += "SET close_ex = ?, dt_c = ? "
        q += "WHERE rowid = ? "
        self._execute(q, (price, date, rowid))

    def archive_trades(self):
        q = "SELECT rowid, * FROM up WHERE dt_o != \"\" AND dt_c != \"\""
        self._execute(q)
        resup = self._get_trades()

        q = "SELECT rowid, * FROM down WHERE dt_o != \"\" AND dt_c != \"\""
        self._execute(q)
        resdown = self._get_trades()

        res = resup + resdown

        for row in res:
            q = "INSERT INTO closed VALUES (?, ?, ?, ?, ?, ?, ?)"
            self._execute(q, row[1:])
            print("Archived: ", row[1:])

        for row in resup:
            q = "DELETE FROM up WHERE rowid = ?"
            self._execute(q, (row[0],))

        for row in resdown:
            q = "DELETE FROM down WHERE rowid = ?"
            self._execute(q, (row[0],))

    def _print_trades(self, row, begin="", end=""):
        if row == "header1":
            row = ("Size", "Open", "Close", "Op Ex", "Cl Ex",
                   "Open Date", "Close Date")
        if row == "header2":
            row = (("========", "========", "========", "========", "========",
                    "===============", "=============="))
        if row == "empty":
            row = (("........", "........", "........", "........", "........",
                   "...............", ".............."))

        print(begin + '{:<8}'.format(row[0]),
              '{:<8}'.format(row[1]),
              '{:<8}'.format(row[2]),
              '{:<8}'.format(row[3]),
              '{:<8}'.format(row[4]),
              '{:<15}'.format(row[5][:15]),
              '{:<15}'.format(row[6][:15]), end=end
              )

    def print_trades(self, t_o=3):
        """ print list of trades

        args:
            t_o: 0 - unopened
                 1 - open date set
                 2 - close date set
                 3 - all (default)
        """

        ut = self.get_trades(self.UP, t_o)
        dt = self.get_trades(self.DOWN, t_o)
        utc = 0
        dtc = 0
        self._print_trades("header1", "     ", "   ")
        self._print_trades("header1", "     ", "\n")
        self._print_trades("header2", "     ", "   ")
        self._print_trades("header2", "     ", "\n")
        while True:
            if(utc < len(ut)):
                self._print_trades(ut[utc][1:],
                                   '{: <5}'.format(str(ut[utc][0])),
                                   "   ")
            else:
                self._print_trades("empty", "     ", "   ")
            if(dtc < len(dt)):
                self._print_trades(dt[dtc][1:],
                                   '{: <5}'.format(str(dt[dtc][0])),
                                   "\n")
            else:
                self._print_trades("empty", "     ", "\n")
            utc += 1
            dtc += 1
            if(utc >= len(ut) and (dtc >= len(dt))):
                break

def main():
    if len(sys.argv) != 2:
        print("Please specify filename as an argument!!")
        exit(0)

    fn = str(sys.argv[1])
    file = fn.split('.')[0] + ".sqlite"

    try:
        with Trades(file) as t:
            # t.create_trade(25, 2000, 1.11, t.DOWN)
            # t.create_trade(25, 2000, 1.11, t.UP)
            # print("Up", t.trades_list(t.UP))
            # print("Down", t.trades_list(t.DOWN))
            # print(t.close_trade(3, t.UP, "today"))
            t.archive_trades()
    except Error as e:
        print("ERROR: ", e)


if __name__ == "__main__":
    main()
