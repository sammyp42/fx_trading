import sys
import argparse
import common.config
import pickle
from candle import Price, Candle
from datetime import datetime
import sqlite3

def get_all_data(conn, api, year):
    cursor = conn.cursor()
    cursor.execute("drop table if exists samp")
    cursor.execute("create table samp (time TEXT PRIMARY_KEY, data TEXT)")
    conn.commit()

    q = "INSERT INTO samp VALUES (?, ?)"

    rawfmt = "%Y-%m-%dT%H:%M:%S.000000000Z"
    fmt = "%Y-%m-%d %H:%M:%S"
    dt = datetime.strptime(year + "-12-25 00:00:00", fmt)
    lastT = ""

    inst = "EUR_USD"
    kwargs = {}
    kwargs["granularity"] = "S5"
    kwargs["price"] = "BMA"
    kwargs["fromTime"] = api.datetime_to_str(dt)
    kwargs["count"] = 5000
    kwargs["includeFirst"] = True

    response = api.instrument.candles(inst, **kwargs)

    fc = []

    for c in response.get("candles", 200):
        bidP = Price(c.bid.c, c.bid.h, c.bid.l, c.bid.o)
        askP = Price(c.ask.c, c.ask.h, c.ask.l, c.ask.o)
        midP = Price(c.mid.c, c.mid.h, c.mid.l, c.mid.o)
        candle = Candle(None, inst, c.time, bidP, askP, midP, c.volume)
        fc.append(candle)
        cursor.execute(q, (c.time, pickle.dumps(candle),))

    conn.commit()
    lastT = str(datetime.strptime(fc[-1].time, rawfmt))

    for i in range(100000):

        print(lastT, i)
        kwargs["includeFirst"] = False

        dt = datetime.strptime(lastT, fmt)
        kwargs["fromTime"] = api.datetime_to_str(dt)

        try:
            response = api.instrument.candles(inst, **kwargs)
        except Exception:
            print(sys.exc_info())
            print("...retrying...")
            continue

        if len(response.get("candles", 200)) == 0:
            break

        if response.get("candles", 200)[0] == lastT:
            break

        fc = []
        for c in response.get("candles", 200):
            lastY = datetime.strptime(c.time, rawfmt).year
            if int(lastY) != int(year):
                break
            bidP = Price(c.bid.c, c.bid.h, c.bid.l, c.bid.o)
            askP = Price(c.ask.c, c.ask.h, c.ask.l, c.ask.o)
            midP = Price(c.mid.c, c.mid.h, c.mid.l, c.mid.o)
            candle = Candle(None, inst, c.time, bidP, askP, midP, c.volume)
            fc.append(candle)
            cursor.execute(q, (c.time, pickle.dumps(candle),))

        if len(fc) == 0:
            break

        lastT = str(datetime.strptime(fc[-1].time, rawfmt))
        conn.commit()

    print(lastT)


def main():
    """
    Create an API context, and use it to fetch and display the state of an
    Account.

    The configuration for the context and Account to fetch is parsed from the
    config file provided as an argument.
    """

    parser = argparse.ArgumentParser()

    #
    # The config object is initialized by the argument parser, and contains
    # the REST APID host, port, accountID, etc.
    #
    common.config.add_argument(parser)

    args = parser.parse_args()

    year = args.year

    #
    # The v20 config object creates the v20.Context for us based on the
    # contents of the config file.
    #
    api = args.config.create_context()

    account_id = args.config.active_account
    response = api.account.get(account_id)

    response = response.get("account", "200")

    #
    # Fetch the details of the Account found in the config file
    #

    db_file = str(int(year)) + "_candles.sqlite"
    conn = sqlite3.connect(db_file)

    get_all_data(conn, api, str(year))

    #    cursor.execute("drop table samp")

    conn.commit()
    conn.close()
    # with open('outfile.can', 'wb') as fp:
    #     pickle.dump(fc, fp)


if __name__ == "__main__":
    main()
