import sys
from trading import Trading
import sqlite3
from sqlite3 import Error
import pickle
from candle import Candle

conn = sqlite3.connect("../data/testdata.sqlite")
cursor = conn.cursor()

def print_prices(bp, sp):
    print("buy to open   :", bp[0], end=";  ")
    print("sell to close  :", sp[1], end=";  ")
    print("sell to open  :", sp[0], end=";  ")
    print("buy to close :", bp[1])
    print("buy", bp, "sell", sp)


def buy_sell(c_buy_p, c_sell_p, buy_p, sell_p, time, t):
    """
    def buy_sell(c_buy_p, c_sell_p, buy_p, sell_p, t):
    """
    buy_up_i = -1
    buy_down_i = -1
    sell_up_i = -1
    sell_down_i = -1

    print("Buy prices up (open up side): ", end="")
    for index, p in enumerate(c_buy_p[0]):
        if p[1] <= buy_p:
            t.open_trade((p, (0, 0)), buy_p, time)
            buy_up_i = index
        print(p[1], end=" ")
    print("")

    print("Sell prices down (open down side): ", end="")
    for index, p in enumerate(c_sell_p[1]):
        if p[1] >= sell_p:
            t.open_trade(((0, 0), p), sell_p, time)
            sell_down_i = index
        print(p[1], end=" ")
    print("")

    print("Sell prices up (close up side): ", end="")
    for index, p in enumerate(c_sell_p[0]):
        if p[1] >= sell_p:
            t.close_trade((p, (0, 0)), sell_p, time)
            sell_up_i = index
        print(p[1], end=" ")
    print("")

    print("Buy prices down (close down side): ", end="")
    for index, p in enumerate(c_buy_p[1]):
        if p[1] <= buy_p:
            t.close_trade(((0, 0), p), buy_p, time)
            buy_down_i = index
        print(p[1], end=" ")
    print("")

    return ((buy_up_i, buy_down_i, sell_up_i, sell_down_i))

def main():
    if len(sys.argv) != 2:
        print("Please sqlite3 db filename as an argument!!")
        exit(0)

    fn = str(sys.argv[1])
    file = fn.split('.')[0] + ".sqlite"

    try:
        q = "SELECT data from samp"
        cursor.execute(q)

        try:
            with Trading(file) as t:
                i = 1
                time = ""

                balance = 1000
                buy_pl = ()
                sell_pl = ()

                c = cursor.fetchone()
                if c:
                    print("Initializing...")
                    can = Candle(pickle.loads(c[0]))
                    t.fill_trades(can.mid.o)
                    buy_pl = t.get_buy_prices()
                    sell_pl = t.get_sell_prices()

                    t.print_trades()

                while True:
                    d = cursor.fetchmany(size=10000)
                    if not d:
                        break

                    print("Starting...")

                    for c in d:
                        can = Candle(pickle.loads(c[0]))

                        buy_p = can.bid.h
                        sell_p = can.ask.l
                        print("Buy: ", buy_p, " Sell: ", sell_p)

                        buy_pl = t.get_buy_prices()
                        sell_pl = t.get_sell_prices()

                        bs = buy_sell(buy_pl, sell_pl, buy_p, sell_p,
                                      can.time, t)

                        print(bs)

                        t.print_trades()

                        input("Enter...")
                        # bp = t.get_buy_prices()
                        # sp = t.get_sell_prices()

                        # print_prices(bp, sp)
                        # print(can.mid.o)
                        # t.print_trades()
                        # input("Enter...")

                        # if i % 25000 == 0:
                        #     print("All trades:")
                        #     can

                        # t.open_trade((bp[0], (0, 0)), "open")
                        # t.open_trade(((0, 0), sp[0]), "open")
                        # print("All open trades:")
                        # t.print_trades(1)
                        # t.close_trade((sp[1], (0, 0)), "close")
                        # t.close_trade(((0, 0), bp[1]), "close")
                        # print("All closed trades:")
                        # t.print_trades(2)
                        # print("\nAll trades:")
                        # t.print_trades()
                        # t.archive_trades()
                        # print("All trades:")
                        # t.print_trades()

                        i += 1

                print_prices(bp, sp)
                print(time)
            conn.close()

        except Error as e:
            print("ERROR: ", e)

    except Error as e:
        print("ERROR: ", e)


if __name__ == "__main__":
    main()
