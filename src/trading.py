import sys
# from datetime import datetime
from trade_db import Trades, Error

class Trading:

    __PIP = 1 / 10000
    _FTP = 50 * __PIP
    _MOV = 25
    _TSZ = 1000

    # Space between trades
    _SBT = 6 * __PIP

    # Maximum open trades
    _MAX = 6

    # Maximum daily profit
    _MDP = 6
    _dtfmt = "%Y-%m-%d %H:%M:%S.000000000Z"

    def __init__(self, file):
        self._t = Trades(file)

        # current open up trades
        self._C_UP = 0

        # current open down trades
        self._C_DOWN = 0

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def fill_trades(self, price):
        ut = self._t.get_trades(self._t.UP, False)
        dt = self._t.get_trades(self._t.DOWN, False)

        if len(ut) > 0:
            fp = ut[-1][2] + self._SBT
        else:
            fp = price

        while fp < price + self._FTP:
            fp = round(fp, 6)
            self._t.create_trade(self._MOV, self._TSZ, fp, self._t.UP)
            fp += self._SBT

        if len(dt) > 0:
            fp = dt[-1][2] - self._SBT
        else:
            fp = price

        while fp > price - self._FTP:
            fp = round(fp, 6)
            self._t.create_trade(self._MOV, self._TSZ, fp, self._t.DOWN)
            fp -= self._SBT

    def get_buy_prices(self):
        ut = self._t.get_trades(self._t.UP, 0)
        dt = self._t.get_trades(self._t.DOWN, 1)

        lu = []
        ld = []

        for t in ut:
            if t[2] > 0:
                lu.append((t[0], t[2]))

        for t in dt:
            if t[3] > 0:
                ld.append((t[0], t[3]))

        return(tuple(lu), tuple(ld))

    def get_sell_prices(self):
        ut = self._t.get_trades(self._t.UP, 1)
        dt = self._t.get_trades(self._t.DOWN, 0)

        lu = []
        ld = []

        for t in ut:
            if t[3] > 0:
                lu.append((t[0], t[3]))

        for t in dt:
            if t[2] > 0:
                ld.append((t[0], t[2]))

        return(tuple(lu), tuple(ld))

    def open_trade(self, idt, price, date):
        """
        idt - index tuple
        price, date
        """
        if (idt[0][0] == 0 and idt[1][0] == 0):
            print(idt, "open trade values funny!")
            return False

        if (idt[0][0] != 0 and idt[1][0] != 0):
            print(idt, "open trade values funny!")
            return False

        if idt[0][0] > 0:
            if self._C_UP >= self._MAX:
                print("Not opened, maximum open UP trades reached!")
                return False
            self._t.open_trade(self._t.UP, idt[0][0], price, date)
            self._C_UP += 1

        if idt[1][0] > 0:
            if self._C_DOWN >= self._MAX:
                print("Not opened, maximum open DOWN trades reached!")
                return False
            self._t.open_trade(self._t.DOWN, idt[1][0], price, date)
            self._C_DOWN += 1

        return True

    def close_trade(self, idt, price, date):
        """
        idt - index tuple
        price, date
        """
        if (idt[0][0] == 0 and idt[1][0] == 0):
            print(idt, "close trade values funny!")
            return False
        if (idt[0][0] != 0 and idt[1][0] != 0):
            print(idt, "close trade values funny!")
            return False
        print("Closing: ", idt)

        if idt[0][0] > 0:
            self._t.close_trade(self._t.UP, idt[0][0], price, date)
            self._C_UP -= 1

        if idt[1][0] > 0:
            self._t.close_trade(self._t.DOWN, idt[1][0], price, date)
            self._C_DOWN -= 1

        return True

    def print_trades(self, t_o=3):
        """ print list of trades

        args:
            t_o: 0 - unopened
                 1 - open date set
                 2 - close date set
                 3 - all (default)
        """
        self._t.print_trades(t_o)

    def archive_trades(self):
        self._t.archive_trades()

def main():
    if len(sys.argv) != 2:
        print("Please sqlite3 db filename as an argument!!")
        exit(0)

    fn = str(sys.argv[1])
    file = fn.split('.')[0] + ".sqlite"

    try:
        with Trading(file) as t:
            t.fill_trades(1.13)
            t.print_trades()
            bp = t.get_buy_prices()
            sp = t.get_sell_prices()
            print("buy to open   :", bp[0])
            print("sell to close  :", sp[1])
            print("sell to open  :", sp[0])
            print("buy to close :", bp[1])
            print("buy", bp, "sell", sp)
            t.open_trade((bp[0], (0, 0)), "open")
            t.open_trade(((0, 0), sp[0]), "open")
            t.print_trades(1)
            t.close_trade((sp[1], (0, 0)), "close")
            t.close_trade(((0, 0), bp[1]), "close")
            t.print_trades(2)
            t.archive_trades()
            t.print_trades()

    except Error as e:
        print("ERROR: ", e)


if __name__ == "__main__":
    main()
