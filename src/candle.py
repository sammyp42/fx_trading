from dataclasses import dataclass
from datetime import datetime

@dataclass
class Price:
    """
    """

    c: float
    h: float
    l: float
    o: float

    __slots__ = ['c', 'h', 'l', 'o']

@dataclass
class Candle:
    """
    inst, time, bid, ask, mid, vol
    -c -h -l -o
    """

    inst: str
    time: datetime

    bid: Price
    ask: Price
    mid: Price

    vol: int
    __slots__ = ['inst', 'time', 'bid', 'ask', 'mid', 'vol']

    def __init__(self, can, inst=None, time=None,
                 bid=None, ask=None, mid=None, vol=None):
        if can is None:
            self.inst = inst
            self.time = time
            self.bid = bid
            self.ask = ask
            self.mid = mid
            self.vol = vol
        else:
            self.inst = can.inst
            self.time = can .time
            self.bid = can.bid
            self.ask = can.ask
            self.mid = can.mid
            self.vol = can.vol
